import { Actions } from 'react-native-router-flux';
import { NAVIGATE_TO_APPS, NAVIGATE_TO_HOME, NAVIGATE_TO_SETTINGS } from '../actions/types';

const INITIAL_STATE = {

}
export default (state= INITIAL_STATE,action)=>{
    switch(action.type){
        case NAVIGATE_TO_HOME:
            Actions.home();
            return state;
        case NAVIGATE_TO_APPS:
            Actions.apps();
            return state;
        case NAVIGATE_TO_SETTINGS:
            Actions.setings();
            return state;
        default:
            return state;
    }
}
