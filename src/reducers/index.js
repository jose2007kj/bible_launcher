import { combineReducers } from 'redux';
import NavigationReducer from './navigationReducer';
import AppListReducer from './appListReducer';
import BibleReducer from './bibleReducer';
//import ContentReducer from './ContentReducer';

export default combineReducers({
    navigation: NavigationReducer,
    appList: AppListReducer,
    verse: BibleReducer
});
