import {START_FETCHING_BIBLE_QUOTES,FETCHING_BIBLE_QUOTES_FAILED,FETCHING_BIBLE_QUOTES_SUCCESSFULL} from '../actions/types';
const INITIAL_STATE = {
    verse: '',
    fetch_status:'',
    error:''
}
export default (state=INITIAL_STATE,action)=>{
    switch(action.type){
        case START_FETCHING_BIBLE_QUOTES:
            return{
                ...state,
                fetch_status:"loading"
            }
        case FETCHING_BIBLE_QUOTES_FAILED:
            return{
                ...state,
                verse:'',
                error:action.payload,
                fetch_status:"failed"
            }
        case FETCHING_BIBLE_QUOTES_SUCCESSFULL:
            return{
                ...state,
                verse:action.payload,
                fetch_status: 'success'
            }
        default:
            return state;
    }
}
