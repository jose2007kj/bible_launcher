import {START_FETCHING_APPS,FETCHING_APPS_SUCCESSFULL,FETCHING_APPS_FAILED,OPEN_SETTINGS_PAGE,OPEN_APP,OPEN_DIALER,OPEN_CAMERA} from '../actions/types';
import {NativeModules} from 'react-native';
const INITIAL_STATE = {
    appList: [],
    fetch_status: '',
    error: ''
}
export default (state=INITIAL_STATE,action) =>{
    switch(action.type){
        case START_FETCHING_APPS:
            console.log("starte fetching applst")
            return{
                ...state,
                fetch_status: 'loading',
                error: ''
            }
        case FETCHING_APPS_SUCCESSFULL:
            return{
                ...state,
                appList:action.payload,
                fetch_status: 'success'
            }
        case FETCHING_APPS_FAILED:
            return{
                ...state,
                fetch_status: 'failed',
                error: ''
            }
        case OPEN_SETTINGS_PAGE:
            NativeModules.InstalledApps.launchApplicationDetails(action.payload)
            return state;
        case OPEN_APP:
            NativeModules.InstalledApps.launchApplication(action.payload)
            return state;

        case OPEN_DIALER:
            NativeModules.InstalledApps.openDialer()
            return state;
        case OPEN_CAMERA:
            NativeModules.InstalledApps.OpenCamera()
            return state;
        default:
            return state;
    }
}
