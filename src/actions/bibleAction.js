import { START_FETCHING_BIBLE_QUOTES,FETCHING_BIBLE_QUOTES_SUCCESSFULL,FETCHING_BIBLE_QUOTES_FAILED } from './types';
import Database from '../utils/database';


const db = new Database();
export const fetchBibleQuotes = () =>{
    return async (dispatch) => {
        dispatch({
          type: START_FETCHING_BIBLE_QUOTES
        })

        db.getRandom().then((data)=>{
            let verse = Object.values(data);
            dispatch({
                    type: FETCHING_BIBLE_QUOTES_SUCCESSFULL,
                    payload: verse[0]+"\n "+verse[3]+" "+verse[2]+":"+verse[1]
            })
               // verse: verse[0]+"\n "+verse[3]+" "+verse[2]+":"+verse[1]
        }).catch((err) => {
            console.log(err);
            dispatch({
                    type: FETCHING_BIBLE_QUOTES_FAILED,
                    payload: 'Failed'
            })
        });
  }

}


