import {NAVIGATE_TO_APPS,NAVIGATE_TO_HOME,NAVIGATE_TO_SETTINGS} from './types';

export const pageNavigation = (screen)=>{
    switch(screen){
        case 'home':
            return {
                type: NAVIGATE_TO_HOME

            }
        case 'settings':
            return {
                type: NAVIGATE_TO_SETTINGS
            }
        case 'apps'
            return {
                type: NAVIGATE_TO_APPS
            }
        default:
            return 0;
    }
}
