import React, { Component } from 'react';
import { Dimensions,StyleSheet, Text, View,NativeModules,Image, TouchableOpacity,ScrollView, ActivityIndicator,AppState } from 'react-native';
import { connect } from 'react-redux';
import {fetchBibleQuotes,fetchInstalledApps,openSettingsPage,openApp,openDialerPage,openCameraPage} from '../actions';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
var width = Dimensions.get('window').width;
var height=Dimensions.get('window').height;
class HomeScreen extends Component {
    constructor(props){
        super(props)
        this.state={
            appList: [],
            appState: AppState.currentState,
            verse: ''
        }

    }componentDidMount(){
        AppState.addEventListener('focus', this._handleAppStateChange);
    }
    componentWillUnmount() {
    AppState.removeEventListener('focus', this._handleAppStateChange);
  }
    _handleAppStateChange = (nextAppState) => {
      console.log('App has come to the foreground!');
        this.props.fetchInstalledApps();

    //this.setState({appState: nextAppState});
  };
       componentWillMount(){
        this.props.fetchBibleQuotes();
        this.props.fetchInstalledApps();
    }
    render() {
        switch(this.props.fetch_status_apps){
         case 'failed':
                console.log("failed!!")
        return (
        <View style={styles.outer}>
         <ActivityIndicator size={'large'} />
            </View>
        )

            default:
        return (
        <View style={styles.outer}>
        <View style={{ alignItems:'center', justifyContent:'center',width:width*0.9, height:height*0.35,marginRight:width*0.01,marginLeft:width*0.01, borderRadius:35, borderColor:'#535353', borderWidth:3, backgroundColor:'#757575'}}><Text style={styles.verse}>{this.props.verse}</Text>
    <Button title="View another"
    onPress={() => this.props.fetchBibleQuotes()}
             buttonStyle={{backgroundColor: "#000000",
      width: width*0.4,
      borderColor: "transparent",
      borderWidth: 0,
      borderRadius: 20}}
      />
</View>
        <ScrollView style={styles.container}>
                {this.props.appList ? this.props.appList.map((app,i)=>{return(
                    <TouchableOpacity key={i} onPress={()=>this.props.openApp(app)}
                    onLongPress={()=>this.props.openSettingsPage(app.name)}
                    style={styles.appListItem}>

                        <Image style={styles.icon} resizeMode={'contain'} source={{uri: "data:image/png;base64," + app.icon}} />
                        <Text style={styles.text}>{app.label}</Text>

                    </TouchableOpacity>
                )}) :null}

            </ScrollView>
            <View style={styles.bottomOverlay}>
            <View style={styles.bottomTiles}>
            <Icon.Button name="phone" size={30} backgroundColor="#000000"  color="#FFFFFF" onPress={() => this.props.openDialerPage()}/>
            </View>
            <View style={styles.bottomTiles}>
            <Text style={styles.text}></Text></View>
            <View style={styles.bottomTiles}>
            <Icon.Button name="camera" backgroundColor="#000000" size={30} color="#FFFFFF" onPress={() => this.props.openCameraPage()}/>
            </View></View>
            </View>
        )
                   }
    }
}

/**
 * special styles for this page
 */
const styles = StyleSheet.create({
container: {
backgroundColor: '#000000',
    marginBottom: '5%'
},
    text: {
        color: 'white',
        fontSize: 20
    },
    icon: {
        marginRight: 16,
        width: 40,
        height: 40
    },
    appListItem: {
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    outer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
        backgroundColor: '#000000'


    },
    verse: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: "center",
        color:"white",
  textAlignVertical: "center",
      },
    buttonView:{
      marginTop:height*0.001,
      marginLeft:width*0.5
    },
    bottomOverlay:{
    height: '10%',
    width: width,
     flexDirection: 'row'
    },
    bottomTiles:{
        marginTop:'1%',
        flex: 1, alignItems: 'center'
    }
});
const mapStateToProps = (state) => {
    return {
        appList: state.appList.appList,
        applist_error: state.appList.error,
        verse:state.verse.verse,
        fetch_status_verse:state.verse.fetch_status,
        fetch_status_apps:state.appList.fetch_status
    };
};
export default connect(mapStateToProps,{fetchBibleQuotes,openCameraPage,openDialerPage,fetchInstalledApps,openSettingsPage,openApp})(HomeScreen);
