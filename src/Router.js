import React, { Component } from "react";
import { Router, Scene, Actions,ActionConst,Drawer} from "react-native-router-flux";
import { StyleSheet,BackHandler } from "react-native";
import AppsScreen from './components/appsScreen';
import { connect } from 'react-redux';
import {fetchInstalledApps} from './actions';
import SettingsScreen from './components/settingsScreen';
import HomeScreen from './components/homeScreen';
const RouterComponent = () => {
  onBackPressed = () => {
      console.log("no to CAB", Actions)
      //console.log("All packages",JSON.parse(NativeModules.InstalledApps.getApps));
      return true
};
  onEnter=()=>{
   // this.props.fetchInstalledApps()
  }
  return (
    <Router
    >
      <Scene key="root" hideNavBar>
        <Scene
          key="home"
          hideNavBar
          component={HomeScreen}
          onEnter={() => this.props.fetchInstalledApps}
          initial
        />
        <Scene
        key="setings"
          component={SettingsScreen}
          hideNavBar        />
        <Scene
        key="apps"
          component={AppsScreen}
          hideNavBar        />
      </Scene>
    </Router>
  );
};


export default connect(null,{fetchInstalledApps})(RouterComponent);
